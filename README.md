# marketplace

Python 3.8
How to run application:
```
$ git clone https://gitlab.com/newmad4/marketplace.git
$ cd marketplace
$ virtualenv -p python3 venv
$ source ./venv/bin/activate
$ pip install -r requirements.txt

Create PostgreSQL DB and User for DB:

$ sudo -u postgres createuser marketplace_user
$ sudo -u postgres createdb marketplace_db
$ sudo -u postgres psql
psql=# ALTER ROLE marketplace_user with encrypted password 'DB_PASSWORD';
psql=# ALTER ROLE marketplace_user SET client_encoding TO 'utf8';
psql=# ALTER ROLE marketplace_user SET timezone TO 'UTC';
psql=# ALTER ROLE marketplace_user CREATEDB;
psql=# GRANT ALL PRIVILEGES ON DATABASE marketplace_db TO marketplace_user;
psql=# \q

Make migration and start server:

$ python manage.py migrate
$ python manage.py runserver
``` 

### SRS

You will need to create a web application. 
The main goal of applica1on - make an authoriza1on system and perform CRUD opera1ons on users and shops applications.
During this assignment you will need to use Django and Django REST Framework,
using API endpoints - as a backend part and Vue.js - as a frontend part.

#### SHOP APPLICATION
A Shop model should have the following fields: shop_id, name and user (owner of
shop).
Many users can be connected to one shop.

#### USERS APPLICATION
App to perform auth logic, and for custom User model

#### DATABASE
You will need to use MySQL or PostgreSQL DB for this project and not the default Sqlite

#### AUTHORIZATION SYSTEM
User should have two different types: USER and ADMIN (you cannot use default
Django’s is_superuser field, so make custom User table, you can change user type
from Django admin). By default User will have a USER type.
User with type USER should be able to register, confirm his email, log in to system
and see his profile (email, username, whatever). He should be able to
change(update) his profile. He cannot access other users, and cannot access to
shops applica1on/endpoints
User with type ADMIN can access all users, can perform CRUD opera1ons with them,
and also can access to shops applica1on and perform opera1ons with them.

