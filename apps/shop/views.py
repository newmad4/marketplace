from rest_framework.generics import RetrieveUpdateDestroyAPIView, CreateAPIView
from rest_framework.permissions import IsAuthenticated

from shop.models import Shop
from shop.permissions import IsOwnerShopOrAdmin
from shop.serializers import ShopSerializer


__all__ = [
    'BaseShopAPIView',
    'ShopCreateAPIView',
    'ShopAPIView'
]


class BaseShopAPIView:
    """
    Base view for shop views which contains necessary attributes
    """
    permission_classes = (IsAuthenticated,)
    queryset = Shop.objects.all()
    serializer_class = ShopSerializer


class ShopCreateAPIView(BaseShopAPIView, CreateAPIView):
    """
    Shop view which get possibility get some shop, update, create or delete
    """


class ShopAPIView(BaseShopAPIView, RetrieveUpdateDestroyAPIView):
    """
    Shop view which get possibility get some shop, update or delete
    """
    permission_classes = (IsAuthenticated, IsOwnerShopOrAdmin)
