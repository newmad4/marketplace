from rest_framework.test import APITestCase
from django.contrib.auth.hashers import make_password
from django.urls import reverse
from rest_framework import status

from model_bakery import baker

from shop.models import Shop
from user.models import User


class ShopTests(APITestCase):
    """
    Test class for test CRUD operations by ADMIN or USER with Shop model
    """

    def setUp(self):
        self.test_user = baker.make(User, email='test.user@email.io', password=make_password('2020TestPassword!'))
        self.test_admin = baker.make(
            User, email='test.admin@email.io', password=make_password('2020TestPassword!'), type="ADMIN"
        )
        self.test_shop = baker.make(Shop)
        self.test_shop.user.add(self.test_user)
        self.test_shop.save()

        self.admin_access_token = self.client.post(
            reverse('jwt-create'),
            data={'email': 'test.admin@email.io', 'password': '2020TestPassword!'},
            format='json'
        ).data['access']
        self.user_access_token = self.client.post(
            reverse('jwt-create'),
            data={'email': 'test.user@email.io', 'password': '2020TestPassword!'},
            format='json'
        ).data['access']

    def test_get_shop_detail_for_admin_success(self):
        """
        Ensure we get 200 response from API which return shop detail
        """
        url = reverse('shop:shop-info', args=[self.test_shop.pk])
        self.client.credentials(HTTP_AUTHORIZATION='JWT ' + self.admin_access_token)
        response = self.client.get(url)
        self.assertEqual(response.status_code, status.HTTP_200_OK)

    def test_get_shop_detail_for_user_success(self):
        """
        Ensure we get 200 response from API which return shop detail
        """
        url = reverse('shop:shop-info', args=[self.test_shop.pk])
        self.client.credentials(HTTP_AUTHORIZATION='JWT ' + self.user_access_token)
        response = self.client.get(url)
        self.assertEqual(response.status_code, status.HTTP_200_OK)

    def test_get_shop_detail_failed(self):
        """
        Ensure we get 401 response from API for UNAUTHORIZED user
        """
        url = reverse('shop:shop-info', args=[self.test_shop.pk])
        response = self.client.get(url)
        self.assertEqual(response.status_code, status.HTTP_401_UNAUTHORIZED)

    def test_update_shop_detail_success(self):
        """
        Ensure we get 200 response from API after Shop info update by ADMIN
        """
        url = reverse('shop:shop-info', args=[self.test_shop.pk])
        self.client.credentials(HTTP_AUTHORIZATION='JWT ' + self.admin_access_token)
        payload = {'name': 'Test shop', 'users': [self.test_user.pk, self.test_admin.pk]}
        response = self.client.put(url, data=payload)
        self.assertEqual(response.status_code, status.HTTP_200_OK)

    def test_delete_shop_success(self):
        """
        Ensure we get 204 response from API after delete Shop by ADMIN
        """
        url = reverse('shop:shop-info', args=[self.test_shop.pk])
        self.client.credentials(HTTP_AUTHORIZATION='JWT ' + self.admin_access_token)
        response = self.client.delete(url)
        self.assertEqual(response.status_code, status.HTTP_204_NO_CONTENT)

    def test_create_shop_success(self):
        """
        Ensure we get 201 response from API after new Shop created by ADMIN
        """
        url = reverse('shop:shop-create')
        self.client.credentials(HTTP_AUTHORIZATION='JWT ' + self.admin_access_token)
        payload = {'name': 'Test shop 2', 'users': [self.test_user.pk]}
        response = self.client.post(url, data=payload)
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)
