from django.utils.translation import ugettext as _
from rest_framework import serializers
from rest_framework.exceptions import ValidationError
from rest_framework.serializers import ModelSerializer

from shop.models import Shop


class ShopSerializer(ModelSerializer):
    users = serializers.ListField(write_only=True, required=True)

    class Meta:
        model = Shop
        fields = ('id', 'name', 'users', )
        read_only_fields = ('id', )

    def create(self, validated_data):
        users = validated_data.pop('users', [])
        if not users:
            raise ValidationError({'users': _('users list field must be not empty.')})
        instance = super().create(validated_data)
        for user_id in users:
            instance.user.add(user_id)
        return instance

    def update(self, instance, validated_data):
        users = validated_data.pop('users', [])
        if not users:
            raise ValidationError({'users': _('users list field must be not empty.')})
        instance.name = validated_data.get('name', instance.name)
        instance.save()
        instance.user.clear()
        for user in users:
            instance.user.add(user)
        return instance

