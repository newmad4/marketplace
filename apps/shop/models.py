from django.db import models
from django.utils.translation import ugettext as _

from user.models import User


class Shop(models.Model):
    """
    Shop model in our service
    """
    name = models.CharField(max_length=200, help_text=_("Shop name"))
    user = models.ManyToManyField(User, related_name="shops", blank=True, help_text=_("Shops owner"))
