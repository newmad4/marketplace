from django.urls import path

from shop.views import ShopAPIView, ShopCreateAPIView

app_name = 'shop'

urlpatterns = [
    path('', ShopCreateAPIView.as_view(), name='shop-create'),
    path('<int:pk>/', ShopAPIView.as_view(), name='shop-info'),
]
