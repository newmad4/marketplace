from rest_framework import permissions

from shop.models import Shop
from user.models import User

__all__ = [
    'IsOwnerShopOrAdmin',
]


class IsOwnerShopOrAdmin(permissions.BasePermission):
    """
    Check if user is owner of shop or he is ADMIN
    """
    def has_object_permission(self, request, view, obj):
        if obj in request.user.shops.all() or request.user.type == User.ADMIN:
            return True
        return False
