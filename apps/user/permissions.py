from rest_framework import permissions

from user.models import User

__all__ = [
    'IsOwnerProfileOrAdmin',
]


class IsOwnerProfileOrAdmin(permissions.BasePermission):
    """
    Check if user has permission or he is ADMIN
    """
    def has_object_permission(self, request, view, obj):
        if request.user == obj or request.user.type == User.ADMIN:
            return True
        return False
