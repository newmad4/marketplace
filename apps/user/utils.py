from django.contrib.auth.tokens import PasswordResetTokenGenerator
from django.utils import six
from django.conf import settings
from django.core.mail import EmailMessage
from django.utils.encoding import force_bytes
from django.utils.http import urlsafe_base64_encode
from django.template.loader import render_to_string


def send_mail_email_confirmation(user):
    mail_subject = 'Confirm you email'
    message = render_to_string('confirm_email.html', {
        'user': user,
        'domain': settings.DOMAIN,
        'uid': urlsafe_base64_encode(force_bytes(user.pk)),
        'token': AccountActivationTokenGenerator().make_token(user),
    })
    email = EmailMessage(mail_subject, message, to=[user.email])
    email.send()


class AccountActivationTokenGenerator(PasswordResetTokenGenerator):
    def _make_hash_value(self, user, timestamp):
        return (
                six.text_type(user.pk) + six.text_type(timestamp) + six.text_type(user.is_active)
        )
