from django.utils.translation import ugettext as _
from rest_framework import status
from rest_framework.generics import RetrieveUpdateDestroyAPIView
from rest_framework.permissions import IsAuthenticated
from rest_framework.response import Response
from rest_framework.views import APIView

from user.models import User
from user.permissions import IsOwnerProfileOrAdmin
from user.serializers import ConfirmCodeSerializer, UserProfileSerializer
from user.utils import send_mail_email_confirmation

__all__ = [
    'UserEmailConfirmAPIView',
    'UserGetUpdateDeleteProfileAPIView',
]


class UserEmailConfirmAPIView(APIView):
    """
    View which allow confirm email for authenticated users
    """
    permission_classes = (IsAuthenticated,)

    def get(self, request, *args):
        """
        Send mail with verification code
        """
        send_mail_email_confirmation(request.user)
        return Response({'message': _('A verification code has been sent to you by mail')}, status=status.HTTP_200_OK)

    def post(self, request, *args, **kwargs):
        """
        Verify user email
        """
        user = request.user
        serializer = ConfirmCodeSerializer(user, data=request.data)
        if serializer.is_valid(raise_exception=True):
            user.is_email_confirmed = True
            user.save()

        return Response({'message': _('Email was successfully verified')}, status=status.HTTP_200_OK)


class UserGetUpdateDeleteProfileAPIView(RetrieveUpdateDestroyAPIView):
    """
    View which get, update or delete user data
    """
    permission_classes = (IsAuthenticated, IsOwnerProfileOrAdmin)
    queryset = User.objects.all()
    serializer_class = UserProfileSerializer
