from rest_framework import serializers
from rest_framework.exceptions import ValidationError
from django.utils.translation import ugettext as _

from user.models import User
from user.utils import AccountActivationTokenGenerator

__all__ = [
    'ConfirmCodeSerializer',
    'UserProfileSerializer'
]


class ConfirmCodeSerializer(serializers.Serializer):
    """
    Confirm code serializer for confirm user email
    """
    token = serializers.CharField(help_text='token', max_length=255, required=True)

    def validate(self, attrs):
        token = attrs['token']
        if not AccountActivationTokenGenerator().check_token(self.instance, token):
            raise ValidationError({'token': _('Token is incorrect.')})

        return attrs


class UserProfileSerializer(serializers.ModelSerializer):
    """
    Serializer for user profile data
    """

    class Meta:
        model = User
        fields = ('id', 'email', 'username', 'avatar', )
        read_only_fields = ('id',)
