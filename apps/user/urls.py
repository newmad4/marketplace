from django.urls import path

from user.views import UserEmailConfirmAPIView, UserGetUpdateDeleteProfileAPIView

app_name = 'user'

urlpatterns = [
    path('<int:pk>/', UserGetUpdateDeleteProfileAPIView.as_view(), name='user-info'),
    path('confirm-email/', UserEmailConfirmAPIView.as_view(), name='confirm-email'),
]
