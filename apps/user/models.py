from django.contrib.auth.base_user import AbstractBaseUser
from django.contrib.auth.models import PermissionsMixin
from django.db import models
from django.utils.translation import ugettext as _
from versatileimagefield.fields import VersatileImageField

from user.managers import UserManager

__all__ = [
    "User",
]


class User(AbstractBaseUser, PermissionsMixin):
    """
    User model in our service
    """
    ADMIN = "ADMIN"
    USER = "USER"
    USER_TYPES = (
        (ADMIN, "Admin"),
        (USER, "User")
    )
    date_joined = models.DateTimeField(auto_now_add=True, help_text=_("When user connect to service"))
    username = models.CharField(max_length=30, null=True, help_text=_("User name for interaction with other users"))
    email = models.EmailField(unique=True, help_text=_("Main user field in system"))
    is_email_confirmed = models.BooleanField(default=False, help_text=_("Did user confirm his email?"))
    type = models.CharField(max_length=20, choices=USER_TYPES, default=USER, help_text=_("User role in system"))
    avatar = VersatileImageField(
        upload_to="images/",
        default="images/avatar-default-icon.png",
        help_text=_("User avatar picture")
    )

    # Service Fields
    is_staff = models.BooleanField(
        verbose_name=_('staff status'),
        default=False,
        help_text=_("Designates whether the user can log into this admin site."),
    )

    is_active = models.BooleanField(
        default=True, help_text=_(
            "Designates whether this user should be treated as active. Unselect this instead of deleting accounts"
        ),
    )
    objects = UserManager()
    USERNAME_FIELD = "email"

    def __str__(self):
        return self.username or self.email.split("@")[0]

    @property
    def is_admin(self):
        return self.type == self.ADMIN
