from rest_framework.test import APITestCase
from django.contrib.auth.hashers import make_password
from django.urls import reverse
from rest_framework import status

from model_bakery import baker

from user.models import User


class UserTests(APITestCase):
    """
    Test class for test some user actions
    """

    def setUp(self):
        self.test_user = baker.make(User, email='test.user@email.io', password=make_password('2020TestPassword!'))

    def test_get_access_token_success(self):
        """
        Ensure we get 200 response with access token
        """
        response = self.client.post(
            reverse('jwt-create'),
            data={'email': self.test_user.email, 'password': '2020TestPassword!'},
            format='json'
        )
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertTrue('access' in response.data)

    def test_user_registration_success(self):
        """
        Ensure we get 201 response from Auth API which register new user.
        """
        url = reverse('user-list')
        payload = {'email': 'test.user_2@email.io', 'password': '2020TestPassword!'}

        # Stop including any credentials
        self.client.credentials()

        response = self.client.post(url, data=payload)
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)
